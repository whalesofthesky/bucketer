#!/usr/bin/env python

import argparse
import os
import os.path
import sys

def main(source, target, buckets, prefix):
    if not os.path.exists(target):
        os.mkdir(target)

    if len(os.listdir(target)):
        raise Exception("directory {} not empty".format(target))

    for bucket in buckets:
        os.mkdir(os.path.join(target, bucket))

    for i, filename in enumerate(os.listdir(source)):
        with open(os.path.join(source, filename), "rb") as image:
            data = image.read()

        for j, bucket in enumerate(buckets):
            if (i - j) % len(buckets) == 0:
                target_bucket = bucket
                break

        new_basename = "{}_{}{}".format(
            prefix,
            i // len(buckets),
            os.path.splitext(filename)[1]
        )

        new_filename = os.path.join(target, target_bucket, new_basename)

        with open(new_filename, "wb") as new_image:
            new_image.write(data)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Copy files from one directory into another where each item is sent to one of n buckets")
    parser.add_argument("--source", "-s", required=True, help="source directory")
    parser.add_argument("--target", "-t", required=True, help="target directory")
    parser.add_argument("--buckets", default="normal,dark,light", help="comma separated list of buckets e.g. 'normal,dark,light'")
    parser.add_argument("--prefix", default="img", help="prefix for filename e.g. the 'img' in 'normal/img_1.jpg'")

    parsed = parser.parse_args(sys.argv[1:])

    main(
        parsed.source,
        parsed.target,
        parsed.buckets.split(","),
        parsed.prefix
    )
